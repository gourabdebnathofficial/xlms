<?php
// Establish database connection
$connection = mysqli_connect("localhost", "root", "", "xlayerlms");

// Check if the connection was successful
if (!$connection) {
    die("Database connection failed: " . mysqli_connect_error());
}

// Prepare the SQL statement to prevent SQL injection
$query = $connection->prepare("INSERT INTO users (id, name, email, password, mobile, address) VALUES (?, ?, ?, ?, ?, ?)");

// Bind parameters
$query->bind_param("isssss",$_POST['id'], $_POST['name'], $_POST['email'], $_POST['password'], $_POST['mobile'], $_POST['address']);

// Execute the query
if ($query->execute()) {
    echo '<script type="text/javascript">
        alert("Registration successful...You may Login now!!");
        window.location.href = "index.php";
    </script>';
} else {
    echo "Error: " . $query->error;
}

// Close the connection
$query->close();
$connection->close();
?>
