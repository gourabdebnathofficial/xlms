<?php
require("functions.php");
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $request_id = $_POST['request_id'];
    $action = $_POST['action']; // 'Approve' or 'Deny'
    update_request_status($request_id, $action);
}

$requests = get_pending_requests();
?>
<!DOCTYPE html>
<html>

<head>
    <title>Dashboard</title>
    <meta charset="utf-8" name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap-4.4.1/css/bootstrap.min.css">
    <script type="text/javascript" src="../bootstrap-4.4.1/js/jquery_latest.js"></script>
    <script type="text/javascript" src="../bootstrap-4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="admin_dashboard.php">xLayer Library Management System (LMS)</a>
            </div>
            <font style="color: white"><span><strong>Welcome: <?php echo $_SESSION['name']; ?></strong></span></font>
            <font style="color: white"><span><strong>Email: <?php echo $_SESSION['email']; ?></strong></font>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown">My Profile </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="view_profile.php">View Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="edit_profile.php">Edit Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="change_password.php">Change Password</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../logout.php">Logout</a>
                </li>
            </ul>
        </div>
    </nav><br>

    <div class="container">
        <h1>Pending Book Requests</h1>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>User ID</th>
                    <th>Book ID</th>
                    <th>Request Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($requests) > 0) {
                    foreach ($requests as $request) {
                        echo "<tr>";
                        echo "<td>" . htmlspecialchars($request['user_id']) . "</td>";
                        echo "<td>" . htmlspecialchars($request['books_id']) . "</td>";
                        echo "<td>" . htmlspecialchars($request['request_date']) . "</td>";
                        echo "<td>";
                        echo '<form method="POST" action="approve_book_request.php">';
                        echo '<input type="hidden" name="request_id" value="' . htmlspecialchars($request['id']) . '">';
                        echo '<button type="submit" name="action" value="Approve" class="btn btn-success">Approve</button>';
                        echo '<button type="submit" name="action" value="Deny" class="btn btn-danger">Deny</button>';
                        echo '</form>';
                        echo "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr><td colspan='4'>No pending requests found.</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
</body>

</html>
