<?php
function get_author_count()
{
	$connection = mysqli_connect("localhost", "root", "");
	$db = mysqli_select_db($connection, "xlayerlms");
	$author_count = 0;
	$query = "select count(*) as author_count from authors";
	$query_run = mysqli_query($connection, $query);
	while ($row = mysqli_fetch_assoc($query_run)) {
		$author_count = $row['author_count'];
	}
	return ($author_count);
}

function get_user_count()
{
	$connection = mysqli_connect("localhost", "root", "");
	$db = mysqli_select_db($connection, "xlayerlms");
	$user_count = 0;
	$query = "select count(*) as user_count from users";
	$query_run = mysqli_query($connection, $query);
	while ($row = mysqli_fetch_assoc($query_run)) {
		$user_count = $row['user_count'];
	}
	return ($user_count);
}

function get_book_count()
{
	$connection = mysqli_connect("localhost", "root", "");
	$db = mysqli_select_db($connection, "xlayerlms");
	$book_count = 0;
	$query = "select count(*) as book_count from books";
	$query_run = mysqli_query($connection, $query);
	while ($row = mysqli_fetch_assoc($query_run)) {
		$book_count = $row['book_count'];
	}
	return ($book_count);
}

function get_issue_book_count()
{
	$connection = mysqli_connect("localhost", "root", "");
	$db = mysqli_select_db($connection, "xlayerlms");
	$issue_book_count = 0;
	$query = "select count(*) as issue_book_count from issued_books";
	$query_run = mysqli_query($connection, $query);
	while ($row = mysqli_fetch_assoc($query_run)) {
		$issue_book_count = $row['issue_book_count'];
	}
	return ($issue_book_count);
}

function get_category_count()
{
	$connection = mysqli_connect("localhost", "root", "");
	$db = mysqli_select_db($connection, "xlayerlms");
	$cat_count = 0;
	$query = "select count(*) as cat_count from category";
	$query_run = mysqli_query($connection, $query);
	while ($row = mysqli_fetch_assoc($query_run)) {
		$cat_count = $row['cat_count'];
	}
	return ($cat_count);
}
function get_pending_requests()
{
	$connection = mysqli_connect("localhost", "root", "", "xlayerlms");
	if (!$connection) {
		die("Connection failed: " . mysqli_connect_error());
	}
	$query = "SELECT id, user_id, books_id, request_date FROM book_requests WHERE status='Pending'";
	$result = mysqli_query($connection, $query);
	$requests = mysqli_fetch_all($result, MYSQLI_ASSOC);
	mysqli_close($connection);
	return $requests;
}

function update_request_status($request_id, $action)
{
	$connection = mysqli_connect("localhost", "root", "", "xlayerlms");
	if (!$connection) {
		die("Connection failed: " . mysqli_connect_error());
	}
	$status = ($action == 'Approve') ? 'Approved' : 'Denied';
	$query = "UPDATE book_requests SET status='$status' WHERE id=$request_id";
	mysqli_query($connection, $query);
	mysqli_close($connection);
}
